<?php
require_once ('Animal.php');
require_once ('Frog.php');
require_once ('Ape.php');


$sheep = new Animal("shaun");

echo "$sheep->name <br>"; // "shaun"
echo "$sheep->legs <br>"; // 2
echo "$sheep->cold_blooded <br><br>";// false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$kodok = new Frog ("buduk");
$kodok->jump()  ; // "hop hop"

echo "<br> $kodok->name <br>"; 
echo "$kodok->legs <br>"; 
echo "$kodok->cold_blooded  <br><br>";

$sungokong = new Ape ("kera sakti");
$sungokong->yell() ;// "Auooo"

echo "<br>$sungokong->name <br>"; 
echo "$sungokong->legs <br>"; 
echo $sungokong->cold_blooded ;


?>


